<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

    class ContactUs
    {
        public static function process($params){
            //date_default_timezone_set( timezone_identifer : "Africa/Lagos");

            //OPEN OR CREATE contact.csv IT NOT EXISTSINTG
            $file = fopen(  "contact.csv",  "a" );

            //CREATE CSV COLUMNS (RUNS THIS ONCE)
           // fputcsv($file, array('Firstname', 'Lastname', 'Areacode', 'Telphone', 'Email', 'Feedback','Date'));

            // APPEND CONTACT DETAILS TO CSV FILE
            fputcsv($file, array($params['firstname'], $params['lastname'], $params['areacode'], $params['telnum'],
                                $params['email'], $params['feedback'], date(  "Y-m-d h:i:sa") ));
            // CLOSE FILE STREAM
            fclose($file);

            //PREPARE EMAIL MESSAGE AND SEND TO USER 
            $message = "Hello ".$params['firstname'].' ' .$params['lastname']."\n\nThanks for reaching out.\nRegards .";
            $headers = "Reply-To: One Movie <one@movies.com>\r\n";
            $headers .= "Return-Path: Movie Admin <one@movies.com>\r\n";
            $headers .= "From: Movie Admin <one@movies.com>\r\n";
            
            //SEND MAIL
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            try {
                //Server settings
                //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'mail username';                 // SMTP username
                $mail->Password = 'your mail password';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                //Recipients
                $mail->setFrom('onemovie@gmail.com', 'One Movie');
                $mail->addAddress($params['email'], $params['firstname'].' ' .$params['lastname']);     // Add a recipient
            //   $mail->addAddress('ellen@example.com');               // Name is optional
                $mail->addReplyTo('one@movies.com', 'One Movie');
            //   $mail->addCC('cc@example.com');
            //  $mail->addBCC('bcc@example.com');

                //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Customer Enquiry';
                $mail->Body    =  $message;
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->send();
                echo 'Message has been sent<br>';
                $csv = array_map('str_getcsv', file('contact.csv'));
                echo 'FullName: '. $csv[sizeof($csv)-1][0]. ' ';
                echo $csv[sizeof($csv)-1][1]. '<br>';
                echo 'Phone Number: '. $csv[sizeof($csv)-1][2];
                echo $csv[sizeof($csv)-1][3]. '<br>';
                echo 'Email: '. $csv[sizeof($csv)-1][4]. '<br>';
                echo 'Message: '. $csv[sizeof($csv)-1][5]. '<br>';
                echo 'Date: '.$csv[sizeof($csv)-1][6]. '<br>';
    
            } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ';
            
            }
           // mail($params['email'], "One Movie", $message, $headers);

            /*
            |---------------------------------
            | READ INFORMATION FROM CSV FILE
            |----------------------------------
            */
            // $scv = array_map( 'str_getscv', file(  'contact.csv'));
            // $csv = array_map('str_getcsv', file('contact.csv'));
            // echo 'FullName: '. $csv[sizeof($csv)-1][0]. ' ';
            // echo $csv[sizeof($csv)-1][1]. '<br>';
            // echo 'Phone Number: '. $csv[sizeof($csv)-1][2];
            // echo $csv[sizeof($csv)-1][3]. '<br>';
            // echo 'Email: '. $csv[sizeof($csv)-1][4]. '<br>';
            // echo 'Message: '. $csv[sizeof($csv)-1][5]. '<br>';
            // echo 'Date: '.$csv[sizeof($csv)-1][6]. '<br>';

              /*
            |---------------------------------
            | DISPLAY USER DETAILS MANUALLY
            |----------------------------------
            */
            // echo "<div class='row' >";
            // echo 'Name: ' . $params['firstname'] .' '. $params['lastname'] . '<br>';
            // echo 'Email: ' . $params['email'] . '<br>' ;
            // echo 'Message: ' . $params['feedback'] . '<br>';
            // echo "</div><br>";


            // $file_handle = fopen("contact.csv", "r");
            //     while (!feof($file_handle) ) {
            //     $line_of_text = fgetcsv($file_handle, 1024);
            //     print $line_of_text[0] . $line_of_text[1]. $line_of_text[2] . "<BR>";
            //     }
            //     fclose($file_handle);
        }
    }

?>