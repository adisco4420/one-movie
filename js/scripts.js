$(document).ready(function(){   
    //for Preloader

    $(window).load(function () {
        $("#loading").fadeOut(2000);
    });
    
    //control courousel play and pause button
    $("#mycarousel").carousel({interval:2000});
    $("#carousel-button").click(function(){
          if ($("#carousel-button").children("span").hasClass('fa-pause')) {
              $("#mycarousel").carousel('pause');
              $("#carousel-button").children("span").removeClass('fa-pause');
              $("#carousel-button").children("span").addClass('fa-play');
          }
          else if ($("#carousel-button").children("span").hasClass('fa-play')){
              $("#mycarousel").carousel('cycle');
              $("#carousel-button").children("span").removeClass('fa-play');
              $("#carousel-button").children("span").addClass('fa-pause');
          }
      });

   
    //show and hide form

    $("#hide").click(function(event){
        event.preventDefault()
        $("#submitForm").hide()
    })

    $("#show").click(function(event){
        event.preventDefault()
        $("#submitForm").show()
    })

    //VALIDATE USER INPUT

    $("#button").click(function formValidate(){

        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var email = $("#emailid").val();
        var feedback = $("#feedback").val()
        var areacode = $("#areacode").val()
        var telnum =  $("#telnum").val()

        //firstname
        if(firstname === ""){
            $('#firstname_error').text('Please enter your Firstname');
           // document.getElementById()
            console.log('nil');      
        }else{
            $('#firstname_error').text('');
        }
        //lastname
        if(lastname === ""){
            $('#lastname_error').text('Please enter your Lastname');
           // document.getElementById()
            console.log('nil');      
        }else{
            $('#lastname_error').text('');
        }

        //email
        if(email === ""){
        $('#email_error').text('Please enter your Email');
        // document.getElementById()
        console.log('nil');      
         }else{
        $('#email_error').text('');
          }

        //Feedback
        if(feedback === ""){
            $('#feedback_error').text('Please enter your Feedback');
            // document.getElementById()
            console.log('nil');      
             }else{
            $('#feedback_error').text('');
              }


        //Area Code
        if(areacode === ""){
            $('#areacode_error').text('Please enter Areacode ');
            // document.getElementById()
            console.log('nil');      
             }else{
            $('#areacode_error').text('');
              }

            //TelNum
            if(telnum === ""){
                $('#telnum_error').text('Please enter your Telephone number');
                // document.getElementById()
                console.log('nil');      
                 }else{
                $('#telnum_error').text('');
                  }
    

            if(email !== "" && firstname !== "" && lastname !== "" && areacode !== "" && telnum !== "" && feedback !==""){
                    firstname = "",
                    lastname = "",
                    email = "",
                    areacode = "",
                    telnum = "",
                    feedback = ""
                var formValues = { 
                    firstname:firstname,
                    lastname:lastname,
                    email:email,
                    areacode:areacode,
                    telnum:telnum,
                    feedback:feedback,
                }
                console.log(formValues);
                $('#feedback_message').text('Thanks for your feedback');
                
            }




        return false;
    
    })
});
