<?php
//start the session
session_start();
include "config/database.php";
if( isset($_POST['email']) && isset($_POST['password'])  ){
    try{
        //select all data
        $query = "SELECT name FROM users WHERE email=:email AND password=:password";
        $stmt = $connection->prepare($query);

        //bind the parameters
        $email = $_POST['email'];
        $password = hash( 'sha256', $_POST['password']);

        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->execute();

        //this is how to get the number of row returned
        $num = $stmt->rowCount();

        if($num>0){
            $row = $stmt->fetch();
            $_SESSION['name'] = $row['name'];
            header("Location: index.php");
            exit();
        }else{
            echo "<div style='margin-top:50px' class='alert alert-danger'>incorrect email or password.</div>";
        }

    }
    //show error
    catch(PDOExeception $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}

else{
    session_destroy();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include "./common/head.php"; ?>
</head>
<body>
<?php 
    //navbar
    include "./common/nav.php";
    include "./common/jumbotron.php";
?>
    <div class="container">
        <div class="row">
            <ol class="col-12 breadcrumb">
                <li class="breadcrumb-item"><a href="./index.html">Home</a></li>
                <li class="breadcrumb-item active">Login</li>
            </ol>
            <div class="col-12">
               <h3>Login</h3>
               <hr>
            </div>
        </div>

              <div class="col-12 col-md-9">
                 <form id="submitForm"   method="post" >
                      <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">email</label>
                          <div class="col-md-10">
                              <input type="email"   class="form-control" id="email" required
                              name="email" placeholder="email">            
                          </div>
                         </div>

                          <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label">Password</label>
                              <div class="col-md-10">
                                  <input type="password"  class="form-control" id="password" required
                                  name="password" placeholder="Password">     
                              </div>
                        </div>

                 
                  
                    <div class="form-group row">
                        <div class="offset-md-2 col-md-10">
                          <button type="submit" id="button" class="btn btn-success btn-md" name="submit">
                          Login
                          </button>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-10">
                            <h5 class="text-primary">Register if you dont have an account <a href="./register.php" type="submit" id="button" class="btn btn-primary btn-sm" name="submit">
                          Register
                          </a></h5>                 
                        </div>
                    </div>

                    </form>
                </div>
    </div>

 <?php 
    //footer
    include "./common/footer.php";
?>




   
   <script src="js/jquery-2.1.4.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.12.1.min.js"></script>		 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<!-- build:js js/main.js -->

<!--  endbuild-->

</body>