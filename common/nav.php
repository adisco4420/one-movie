<nav class="navbar navbar-expand-lg navbar-inverse fixed-top">
        <a class="navbar-brand" href="#"><img src="img/cinema.png" height="30" width="41"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          
          <span class="navbar-toggler-icon"></span>
          <i class="fa fa-align-justify"></i>
        </button>
  
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
              <a class="nav-link" href="index.php">Home  <i class="fa fa-home fa-lg"></i> <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tv-series.php">Tv-Series <i class="fa fa-tv fa-lg"></i> </a>
            </li>
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Genres
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Comedy</a>
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Crime</a>
                <a class="dropdown-item" href="#">Romance</a>
                <a class="dropdown-item" href="#">War</a>
                <a class="dropdown-item" href="#">Sport</a>
              </div>
            </li> -->
           
            <li class="nav-item">
              <a class="nav-link" href="gallery.php">Gallery <span class="fa fa-list fa-lg"></span> </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contactus.php">Contact Us  <span class="fa fa-address-card fa-lg"></span>  </a>
              </li>
     
          </ul>
       
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success btn-sm" type="submit">Search</button>
          </form>
        </div>
      </nav>
      


       <!-- Footer -->







