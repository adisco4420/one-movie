<?php
//start the session
session_start();

//include database connection
include "config/database.php";

if( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password'])  ){
    try{
        //select all data
        $query = "INSERT INTO  users SET name=:name, email=:email, password=:password, created_at=:created_at";
        
        // prepare query for connection
        $stmt = $connection->prepare($query);

        //post value
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $date = date("Y-m-d h:i:sa");
        $passwordhash = hash('sha256', $password);
       

        // bind the parameters
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password',  $passwordhash);
        $stmt->bindParam(':created_at', $date );
        
        

        //execute the query
        if($stmt->execute()){

            $_SESSION['name'] = $name;
            header("Location: index.php");
            exit();

        }else{
            echo "<div style='margin-top:50px'  class='alert alert-danger'>Unable to save record.</div>";
        }
    }
    //show error
    catch(PDOExeception $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}

else{
    session_destroy();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include "./common/head.php"; ?>
</head>
<body>
<?php 
    //navbar
    include "./common/nav.php";
    include "./common/jumbotron.php";
?>
<div class="container">
        <div class="row">
            <ol class="col-12 breadcrumb">
                <li class="breadcrumb-item"><a href="./index.html">Home</a></li>
                <li class="breadcrumb-item active">Register</li>
            </ol>
            <div class="col-12">
               <h3>Register</h3>
               <hr>
            </div>
        </div>

              <div class="col-12 col-md-9">
                 <form id="submitForm"  method="post" >
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">name</label>
                            <div class="col-md-10">
                                <input type="text"   class="form-control" id="name" required
                                name="name" placeholder="Full Name">
                                          
                            </div>
                    </div>


                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                          <input type="email"  class="form-control" id="emailid" required
                          name="email" placeholder="Email">
                     
                      </div>
                      </div>

                   

                          <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label">Password</label>
                              <div class="col-md-10">
                                  <input type="password"  class="form-control" id="password" required
                                  name="password" placeholder="Password">     
                              </div>
                        </div>

                         

                 
                  
                    <div class="form-group row">
                        <div class="offset-md-2 col-md-10">
                          <button type="submit" id="button" class="btn btn-success btn-md" name="submit">
                          Register
                          </button>
                        </div>
                    </div>

                      <div class="row" >
                        <div class="col-md-10">
                            <h5 class="text-primary">Sigin if you have an account <a href="./login.php" type="submit" id="button" class="btn btn-primary btn-sm" name="submit">
                          Login
                          </a></h5>                 
                        </div>
                    </div>

                    </form>
                </div>
    </div>

 <?php 
    //navbar
    include "./common/footer.php";
?>




   
   <script src="js/jquery-2.1.4.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.12.1.min.js"></script>		 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<!-- build:js js/main.js -->

<!--  endbuild-->

</body>