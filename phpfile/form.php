<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>form</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
  <script src="main.js"></script>
</head>
<body>
<?php 
                
                $FnameErr = '';
                $LnameErr = '';
                $EmailErr = '';
                $MsgErr = '';
                $Fname = '';
                $Lname = '';
                $email = '';
                $message = '';
                
                function clean_text($string)
                {
                  $string = trim($string);
                  $string = stripslashes($string);
                  $string = htmlspecialchars($string);
                  return $string;
                }
                
                if(isset($_POST["submit"]))
                {
                  //check first name
                  if(empty($_POST["firstname"]))
                  {
                    $FnameErr .= '<p><label class="text-danger">Please Enter your FirstName</label></p>';
                  }
                  else
                  {
                    $Fname = clean_text($_POST["firstname"]);
                    if(!preg_match("/^[a-zA-Z ]*$/",$Fname))
                    {
                      $FnameErr .= '<p><label class="text-danger">Only letters and white space allowed</label></p>';
                    }
                  }
                  //check last name
                  if(empty($_POST["lastname"]))
                  {
                    $LnameErr .= '<p><label class="text-danger">Please Enter your LastName</label></p>';
                  }
                  else
                  {
                    $Lname = clean_text($_POST["lastname"]);
                    if(!preg_match("/^[a-zA-Z ]*$/",$Lname))
                    {
                      $LnameErr .= '<p><label class="text-danger">Only letters and white space allowed</label></p>';
                    }
                  }

                  if(empty($_POST["email"]))
                  {
                    $EmailErr .= '<p><label class="text-danger">Please Enter your Email</label></p>';
                  }
                  else
                  {
                    $email = clean_text($_POST["email"]);
                    if(!filter_var($email, FILTER_VALIDATE_EMAIL))
                    {
                      $EmailErr .= '<p><label class="text-danger">Invalid email format</label></p>';
                    }
                  }
                  if(empty($_POST["feedback"]))
                  {
                    $MsgErr .= '<p><label class="text-danger">Message is required</label></p>';
                  }
                  else
                  {
                    $message = clean_text($_POST["feedback"]);
                  }
                
                  if($FnameErr == '' &&  $LnameErr == '' && $EmailErr == '' && $MsgErr == ''){
                      //IMPORT YOUR CLASS HERE
                      include "classes/ContactUs.php";

                      //EXECUTE WITH THE CUSTOM STATIC FUNCTION
                      ContactUs::process($_POST);
                     
                      $FnameErr = '';
                      $LnameErr = '';
                      $EmailErr = '';
                      $MsgErr = '';
                      $name = '';
                      $email = '';
                      $message = '';

                      echo '<div class="col-12 col-md-12 well bg-success text-center">
                      <h1>Thank you For Contacting Us</h1>
                      <p>An email has been forwared to you</p>
                      <br>
                      <a href="contactus.php">Contact Us</a>
                      </div>';
                  }
                       
                }
          ?>
          <!-- <div class="col-12 col-md-12 well bg-success text-center">
                  <h1>Thank you For Contacting Us</h1>
                  <p>An email has been forwared to you</p>
                  <br>
                  <a href="contactus.php">Contact Us</a>
          </div> -->
             
                <div class="col-12 col-md-9">
                  <form  method="post" >
                    <div class="form-group row">
                      <label for="firstname" class="col-md-2 col-form-label">Firstname</label>
                        <div class="col-md-10">
                            <input type="text" value="<?php echo $Fname; ?>" class="form-control" id="firstname"
                            name="firstname" placeholder="First Name">
                            <span id="firstname_error"></span>
                            <?php echo $FnameErr;?>
                        </div>
                    </div>

                        <div class="form-group row">
                          <label for="lastname" class="col-md-2 col-form-label">Lastname</label>
                            <div class="col-md-10">
                                <input type="text" value="<?php echo $Lname; ?>" class="form-control" id="lastname"
                                name="lastname" placeholder="Last Name">
                                <span id="lastname_error"></span>
                                <?php echo $LnameErr;?>
                            </div>

                    </div>
                    <div class="form-group row">
                      <label for="telnum" class="col-md-2 col-form-label">Contact Tel.</label>
                      <div class="col-5 col-sm-4 col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon"></div>
                            <input type="tel" class="form-control" id="areacode"
                            name="areacode"  maxlength="4" placeholder="Area code">
                            <span id="areacode_error"></span>
                          <div class="input-group-addon"></div>
                        </div>
                      </div>
                      <div class="col-7 col-sm-6 col-md-7">
                        <input type="tel" class="form-control" maxlength="10" id="telnum"
                        name="telnum" placeholder="Tel number.">
                        <span id="telnum_error"></span>
                    </div>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="col-md-2 col-form-label">Email</label>
                      <div class="col-md-10">
                        <input type="emailid" class="form-control" id="emailid"
                        name="email" placeholder="Email">
                        <span id="email_error"></span>
                        <?php echo $EmailErr;?>
                    </div>
                    </div>
                  
                    <div class="form-group row">
                      <label for="feedback" class="col-md-2 col-form-label">Your Feedback</label>
                      <div class="col-md-10">
                        <textarea  class="form-control" id="feedback"
                        name="feedback" placeholder="how do you feel about oue service" rows="12"></textarea>
                        <span id="feedback_error"></span>
                        <?php echo $MsgErr ?>
                    </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-md-2 col-md-10">
                        <button type="submit" id="button" class="btn btn-success btn-md" name="submit">
                        Submit
                        </button>
                        
                        
                        <span id="feedback_message" class="text-success"></span>
                      </div>
                    </div>
                  </form>
              </div>
</body>
</html>